package com.eurisko.joseph.newyorktimes.models

import com.google.gson.annotations.SerializedName

import java.io.Serializable

class MediaMetadata : Serializable {

    @SerializedName("url")
    var url = ""
    @SerializedName("format")
    var format = ""
    @SerializedName("height")
    var height: Int? = null
    @SerializedName("width")
    var width: Int? = null

}
