package com.eurisko.joseph.newyorktimes.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v4.widget.DrawerLayout

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.WindowManager
import android.widget.TextView

import com.eurisko.joseph.newyorktimes.R
import com.eurisko.joseph.newyorktimes.adapters.NewsAdapter
import com.eurisko.joseph.newyorktimes.adapters.OnNewsClickListener
import com.eurisko.joseph.newyorktimes.Networking.Endpoints.NewsAPI
import com.eurisko.joseph.newyorktimes.models.News
import com.eurisko.joseph.newyorktimes.models.NewsResponse
import com.eurisko.joseph.newyorktimes.models.ResultsArray
import com.eurisko.joseph.newyorktimes.Networking.APIClient
import com.eurisko.joseph.newyorktimes.utils.checkNetwork
import com.eurisko.joseph.newyorktimes.utils.printException
import com.eurisko.joseph.newyorktimes.utils.printThrowable
import com.eurisko.joseph.newyorktimes.utils.showDialogActions

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    //region Variables Declaration

    //Views Variables
    private lateinit var srlNews: SwipeRefreshLayout;
    private lateinit var rvNews: RecyclerView
    private lateinit var tvNoNews: TextView

    //Global Variables
    internal var isRefreshing: Boolean? = false

    //API Variables
    private lateinit var newsApi: NewsAPI
    private var newsResultBeanCall: Call<NewsResponse>? = null

    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
        implementView()
        fetchNews()
    }

    //region Setting up views

    private fun setupView() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        srlNews = findViewById(R.id.srlNews)
        rvNews = findViewById(R.id.rvNews)
        tvNoNews = findViewById(R.id.tvNoNews)
    }

    private fun implementView() {
        rvNews.adapter = NewsAdapter(this, NewsResponse(), null)
        srlNews.setOnRefreshListener { fetchNews() }
    }

    //endregion

    //region API call + handling response

    private fun fetchNews() {
        if (checkNetwork(this)) {
            if ((isRefreshing == false)) {
                isRefreshing = true
                srlNews.isRefreshing = true
                this.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                newsApi = APIClient.getClient("http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/").create(NewsAPI::class.java)

                newsResultBeanCall = newsApi.getNews("GwILdjB5i4TAGGBAFYsksG4pqi0qlrOc")

                newsResultBeanCall!!.enqueue(object : Callback<NewsResponse> {
                    override fun onResponse(call: Call<NewsResponse>, response: Response<NewsResponse>) {
                        srlNews.isRefreshing = false
                        this@MainActivity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        isRefreshing = false
                        if (response.isSuccessful && response.body()!!.status == "OK") {
                            handleResponse(response.body())
                        } else {
                            tvNoNews.visibility = View.VISIBLE
                        }
                    }

                    override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                        this@MainActivity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        if (!call.isCanceled) {
                            srlNews.isRefreshing = false
                            isRefreshing = false

                            showDialogActions(this@MainActivity, getString(R.string.no_internet_msg), getString(R.string.retry_msg), getString(R.string.cancel_msg), Runnable { fetchNews() }, null)

                            printThrowable(t)
                        }
                    }
                })
            }
        } else {
            showDialogActions(this@MainActivity, getString(R.string.no_internet_msg), getString(R.string.retry_msg), getString(R.string.cancel_msg), Runnable { fetchNews() }, null)
        }
    }

    private fun handleResponse(newsResultBean: NewsResponse?) {
        if (newsResultBean != null) {
            try {
                val newsAdapter = NewsAdapter(this, newsResultBean, object: OnNewsClickListener {
                    override fun onNewsClick(resultsArrayBean: ResultsArray) {
                        val intent = Intent(this@MainActivity, NewsDetailsActivity::class.java)
                        intent.putExtra(BUNDLE_NEWS_RESULT, News(resultsArrayBean));
                        this@MainActivity.startActivity(intent)
                    }
                })

                rvNews.layoutManager = LinearLayoutManager(this@MainActivity)
                rvNews.adapter = newsAdapter
            } catch (e: Exception) {
                printException(e)
            }

        }
    }

    //endregion

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (newsResultBeanCall != null) {
            newsResultBeanCall!!.cancel()
        }
    }

    companion object {
        val BUNDLE_NEWS_RESULT = "BundleNewsResult"
    }
}
