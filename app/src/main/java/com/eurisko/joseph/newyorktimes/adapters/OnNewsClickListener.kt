package com.eurisko.joseph.newyorktimes.adapters


import com.eurisko.joseph.newyorktimes.models.ResultsArray

interface OnNewsClickListener {
    fun onNewsClick(resultsArrayBean: ResultsArray)
}
