package com.eurisko.joseph.newyorktimes.models

import android.os.Parcel
import android.os.Parcelable

class News : Parcelable {

    var byLine: String
    var title: String
    var publishedDate: String
    var abstractText: String
    var imageUrl: String

    constructor(resultsArrayBean: ResultsArray) {
        byLine = resultsArrayBean.byline
        title = resultsArrayBean.title
        publishedDate = resultsArrayBean.publishedDate
        abstractText = resultsArrayBean.abstractText
        imageUrl = resultsArrayBean.media[0].mediaMetadata[0].url
    }

    private constructor(parcel: Parcel) {
        val data = arrayOfNulls<String>(5)

        parcel.readStringArray(data)

        byLine = data[0]!!
        title = data[1]!!
        publishedDate = data[2]!!
        abstractText = data[3]!!
        imageUrl = data[4]!!
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeStringArray(arrayOf(byLine, title, publishedDate, abstractText, imageUrl))
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<News> = object : Parcelable.Creator<News> {
            override fun createFromParcel(parcel: Parcel) = News(parcel)
            override fun newArray(size: Int) = arrayOfNulls<News>(size)
        }
    }
}
