package com.eurisko.joseph.newyorktimes.utils

import android.graphics.Bitmap
import com.nostra13.universalimageloader.core.DisplayImageOptions

fun getImageOptions(defaultIcon: Int): DisplayImageOptions {
    return DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .showImageOnLoading(defaultIcon)
            .showImageOnFail(defaultIcon)
            .showImageForEmptyUri(defaultIcon)
            .build()
}