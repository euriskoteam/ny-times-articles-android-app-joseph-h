package com.eurisko.joseph.newyorktimes.Networking.Endpoints

import com.eurisko.joseph.newyorktimes.models.NewsResponse

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsAPI {

    /**
     *
     */
    @GET("7.json?")
    fun getNews(@Query("api-key") key: String): Call<NewsResponse>

}