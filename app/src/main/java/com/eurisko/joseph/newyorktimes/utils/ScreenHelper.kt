package com.eurisko.joseph.newyorktimes.utils

import android.app.Activity
import android.util.DisplayMetrics

fun getScreenWidth(a: Activity): Int {
    val displayMetrics = DisplayMetrics()
    a.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels

}