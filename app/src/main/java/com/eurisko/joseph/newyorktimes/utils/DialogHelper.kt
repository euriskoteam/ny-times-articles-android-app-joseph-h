package com.eurisko.joseph.newyorktimes.utils

import android.app.Activity
import android.app.AlertDialog
import android.os.Handler

fun showDialogActions(activity: Activity, msg: String, positiveText: String, negativeText: String, positiveRunnable: Runnable, negativeRunnable: Runnable?): AlertDialog? {
    val dialog = AlertDialog.Builder(activity)
    dialog.setMessage(msg)
    dialog.setCancelable(false)
    dialog.setPositiveButton(positiveText) { dialog, which ->
        dialog.dismiss()
        Handler().post(positiveRunnable)
    }
    dialog.setNegativeButton(negativeText) { dialog, which ->
        dialog.dismiss()
        Handler().post(negativeRunnable)
    }
    return if (activity.isFinishing) null else dialog.show()
}