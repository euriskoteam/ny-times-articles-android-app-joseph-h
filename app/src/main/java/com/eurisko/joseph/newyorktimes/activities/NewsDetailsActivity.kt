package com.eurisko.joseph.newyorktimes.activities

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.eurisko.joseph.newyorktimes.R
import com.eurisko.joseph.newyorktimes.models.News
import com.eurisko.joseph.newyorktimes.models.ResultsArray
import com.eurisko.joseph.newyorktimes.utils.*
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener

class NewsDetailsActivity : AppCompatActivity() {

    //region Variables declaration

    //Views Variables
    private lateinit var tvNewsTitle: TextView
    private lateinit var tvNewsDate: TextView
    private lateinit var tvNewsBy: TextView
    private lateinit var tvNewsDescription: TextView
    private lateinit var ivNews: ImageView

    //External component variable
    private lateinit var loader: ImageLoader
    private lateinit var options: DisplayImageOptions

    //API Variables
    internal var newsParcelable: News? = null

    //Variables
    internal var screenWidth: Int = 0

    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)

        handleBundles()
        setupView()
        implementView()
    }

    //region Setting up views

    private fun handleBundles() {
        try {
            newsParcelable = intent.extras!!.getParcelable(BUNDLE_NEWS_RESULT)
        } catch (e: Exception) {
            printException(e)
        }

        if (newsParcelable == null) {
            newsParcelable = News(ResultsArray())
        }
    }

    private fun setupView() {
        screenWidth = getScreenWidth(this)
        loader = getImageLoader(this)
        options = getImageOptions(R.drawable.placeholder)
        tvNewsTitle = findViewById(R.id.tvNewsTitle)
        tvNewsDate = findViewById(R.id.tvNewsDate)
        tvNewsBy = findViewById(R.id.tvNewsBy)
        tvNewsDescription = findViewById(R.id.tvNewsDescription)
        ivNews = findViewById(R.id.ivNews)
    }

    private fun implementView() {
        tvNewsTitle.text = newsParcelable!!.title
        tvNewsBy.text = newsParcelable!!.byLine
        tvNewsDate.text = newsParcelable!!.publishedDate
        tvNewsDescription.text = newsParcelable!!.abstractText

        loader.displayImage(newsParcelable!!.imageUrl, ivNews, options, object : ImageLoadingListener {
            override fun onLoadingStarted(imageUri: String, view: View) {
                //Not used
            }

            override fun onLoadingFailed(imageUri: String, view: View, failReason: FailReason) {
                //Not used
            }

            override fun onLoadingComplete(imageUri: String, view: View, loadedImage: Bitmap) {
                try {
                    ivNews.layoutParams.height = screenWidth * loadedImage.height / loadedImage.width
                } catch (e: Exception) {
                    printException(e)
                }

            }

            override fun onLoadingCancelled(imageUri: String, view: View) {
                //Not used
            }
        })
    }

    companion object {

        //Global Variables
        val BUNDLE_NEWS_RESULT = "BundleNewsResult"
    }

    //endregion
}
