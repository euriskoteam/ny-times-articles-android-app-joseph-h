package com.eurisko.joseph.newyorktimes.adapters

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView

import com.eurisko.joseph.newyorktimes.R
import com.eurisko.joseph.newyorktimes.models.NewsResponse
import com.eurisko.joseph.newyorktimes.models.ResultsArray
import com.eurisko.joseph.newyorktimes.customviews.CustomNewsImageView
import com.eurisko.joseph.newyorktimes.utils.getImageLoader
import com.eurisko.joseph.newyorktimes.utils.getImageOptions
import com.eurisko.joseph.newyorktimes.utils.printException
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader

import java.util.ArrayList

class NewsAdapter(activity: Activity, newsResultBean: NewsResponse, listener: OnNewsClickListener?) : RecyclerView.Adapter<NewsAdapter.NewsHolder>() {

    //region Variables Declaration

    //Global Variables
    private var inflater: LayoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var resultsArrayBeans: ArrayList<ResultsArray> = newsResultBean.results

    //External Components
    private var loader: ImageLoader = getImageLoader(activity)
    private var options: DisplayImageOptions = getImageOptions(R.drawable.placeholder)

    var mListener: OnNewsClickListener? = listener

    //endregion

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NewsHolder {
        val itemView = inflater.inflate(R.layout.news_row, viewGroup, false)
        return NewsHolder(itemView)
    }

    override fun onBindViewHolder(newsHolder: NewsHolder, i: Int) {
        val bean = resultsArrayBeans[i]

        newsHolder.tvNewsTitle.text = bean.title
        newsHolder.tvNewsBy.text = bean.byline
        newsHolder.tvNewsDate.text = bean.publishedDate

        loader.displayImage(bean.media[0].mediaMetadata[0].url, newsHolder.civNews, options)

        newsHolder.rlNews.tag = i
        newsHolder.rlNews.setOnClickListener { view ->
            try {
                val pos = view.tag as Int
                val selectedNewsBean = resultsArrayBeans[pos]
                mListener?.onNewsClick(selectedNewsBean)
            } catch (e: Exception) {
                printException(e)
            }
        }
    }

    override fun getItemCount(): Int {
        return resultsArrayBeans.size
    }

    class NewsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var rlNews: RelativeLayout = itemView.findViewById(R.id.rlNews)
        internal var tvNewsTitle: TextView = itemView.findViewById(R.id.tvNewsTitle)
        internal var tvNewsDate: TextView = itemView.findViewById(R.id.tvNewsDate)
        internal var tvNewsBy: TextView = itemView.findViewById(R.id.tvNewsBy)
        internal var civNews: CustomNewsImageView = itemView.findViewById(R.id.civNews)

    }

}
