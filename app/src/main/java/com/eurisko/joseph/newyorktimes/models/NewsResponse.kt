package com.eurisko.joseph.newyorktimes.models


import com.google.gson.annotations.SerializedName

import java.io.Serializable
import java.util.ArrayList

class NewsResponse : Serializable {

    @SerializedName("status")
    var status = ""
    @SerializedName("copyright")
    var copyright = ""
    @SerializedName("num_results")
    var numResults: Int? = null
    @SerializedName("results")
    var results = ArrayList<ResultsArray>()

}
