package com.eurisko.joseph.newyorktimes.utils

import android.util.Log
import com.eurisko.joseph.newyorktimes.BuildConfig

fun printException(e: Exception?) {
    if (BuildConfig.DEBUG && e != null) {
        Log.d("Exception", Log.getStackTraceString(e))
    }
}

fun printThrowable(e: Throwable?) {
    if (BuildConfig.DEBUG && e != null) {
        Log.d("Exception", Log.getStackTraceString(e))
    }
}