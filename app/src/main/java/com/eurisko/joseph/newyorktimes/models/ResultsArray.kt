package com.eurisko.joseph.newyorktimes.models

import com.google.gson.annotations.SerializedName

import java.io.Serializable
import java.util.ArrayList

class ResultsArray : Serializable {

    @SerializedName("url")
    var url = ""
    @SerializedName("adx_keywords")
    var adxKeywords = ""
    @SerializedName("column")
    var column = ""
    @SerializedName("section")
    var section = ""
    @SerializedName("byline")
    var byline = ""
    @SerializedName("type")
    var type = ""
    @SerializedName("title")
    var title = ""
    @SerializedName("abstract")
    var abstractText = ""
    @SerializedName("published_date")
    var publishedDate = ""
    @SerializedName("source")
    var source = ""
    @SerializedName("id")
    var id: Float? = null
    @SerializedName("asset_id")
    var assetId: Float? = null
    @SerializedName("views")
    var views: Int? = null
    @SerializedName("des_facet")
    var desFacet: Any? = null
    @SerializedName("org_facet")
    var orgFacet: Any? = null
    @SerializedName("per_facet")
    var perFacet: Any? = null
    @SerializedName("geo_facet")
    var geoFacet: Any? = null
    @SerializedName("media")
    var media = ArrayList<Media>()

}
