package com.eurisko.joseph.newyorktimes.utils

import android.app.Activity
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration

fun getImageLoader(activity: Activity): ImageLoader {
    val imageLoader = ImageLoader.getInstance()
    if (!imageLoader.isInited) {
        imageLoader.init(ImageLoaderConfiguration.createDefault(activity))
    }
    return imageLoader
}