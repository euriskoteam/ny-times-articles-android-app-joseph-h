package com.eurisko.joseph.newyorktimes.Networking

import com.eurisko.joseph.newyorktimes.utils.printException

import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit

import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.X509Certificate

/**
 * Retrofit API client implementation
 * @author Joseph
 */
object APIClient {

    fun getClient(url: String): Retrofit {

        val builder = OkHttpClient.Builder()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(interceptor)

        try {
            val trustAllCerts = getTrustedManager()

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("TLSv1.2")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier { hostname, session -> hostname.equals(session.peerHost, ignoreCase = true) }

        } catch (e: Exception) {
            printException(e)
        }

        val client = builder.connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS).build()

        return Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

    }

    /**
     * Create a trust manager that does not validate certificate chains
     */
    private fun getTrustedManager(): Array<TrustManager> {
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                try {
                    chain[0].checkValidity()
                } catch (e: Exception) {
                    throw CertificateException("Certificate not valid or trusted.")
                }

            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                try {
                    chain[0].checkValidity()
                } catch (e: Exception) {
                    throw CertificateException("Certificate not valid or trusted.")
                }

            }

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }
        })
        return trustAllCerts
    }
}
