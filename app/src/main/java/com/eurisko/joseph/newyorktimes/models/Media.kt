package com.eurisko.joseph.newyorktimes.models

import com.google.gson.annotations.SerializedName

import java.io.Serializable
import java.util.ArrayList

class Media : Serializable {

    @SerializedName("type")
    var type = ""
    @SerializedName("subtype")
    var subtype = ""
    @SerializedName("caption")
    var caption = ""
    @SerializedName("copyright")
    var copyright = ""
    @SerializedName("approved_for_syndication")
    var approvedForSyndication: Int? = null
    @SerializedName("media-metadata")
    var mediaMetadata = ArrayList<MediaMetadata>()

}