package com.eurisko.joseph.newyorktimes

import android.app.Activity
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView

import com.eurisko.joseph.newyorktimes.activities.MainActivity

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class IntrumentedEspressoTest {

    var count = 0
    lateinit var activity: Activity
    lateinit var rvNews: RecyclerView

    @Rule @JvmField var activityTestRule = ActivityTestRule(MainActivity::class.java)


    @Before
    fun setup() {
        this.activity = activityTestRule.activity
        this.rvNews = activity.findViewById(R.id.rvNews)
    }


    @Test
    fun NewsItemsClickTest() {

        this.activity = activityTestRule.activity

        try {
            Thread.sleep(3000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }


        this.rvNews = activity.findViewById(R.id.rvNews)
        if (rvNews.adapter != null) {
            this.count = rvNews.adapter!!.itemCount
            for (i in 0 until count) {
                Espresso.onView(ViewMatchers.withId(R.id.rvNews))
                        .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(i, ViewActions.click()))
                Espresso.pressBack()
                if (i == 2) {
                    break
                }
            }
        }
    }
}
